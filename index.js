import openid from 'express-openid-connect';
import express from 'express'
import { randomUUID } from 'crypto'
import { config } from './config.js'
import { getKeycloakClient, getUser, setupKeycloak } from './keycloak.js';
import path from 'path'
import { dirname } from 'path';
import { fileURLToPath } from 'url';
import bodyParser from 'body-parser';

const __dirname = dirname(fileURLToPath(import.meta.url));
const app = express()
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

const kcMiddleware = openid.auth({
  issuerBaseURL: `${config.keycloakUrl}/realms/${config.realm}`,
  baseURL: config.apiUrl,
  clientID: config.apiClient.clientId,
  // Not really secure to use a UUID but don't want to get into crypto for this poc
  secret: randomUUID(),
  idpLogout: true,
  session: {
    name: 'kc-poc',
    cookie: {
      secure: false,
    },
  },
  authRequired: true,
  errorOnRequiredAuth: false,
});

const loggingMiddleware = (req, res, next) => {
  console.log(`${req.method} ${req.url}`)
  next();
}

app.use(bodyParser.urlencoded())
app.use(loggingMiddleware);
app.use(kcMiddleware);

// Middleware to make the `user` object available for all views
app.use(function (req, res, next) {
  res.locals.user = req.oidc.user;
  next();
});


app.get('/', (req, res) => {
  res.render('index', {
    title: 'Keycloak Rightcrowd PoC',
    isAuthenticated: req.oidc.isAuthenticated()
  });
});

app.get('/profile', openid.requiresAuth(), async function (req, res, next) {
  const user = await getUser(req.oidc.user.sub)

  res.render('profile', {
    userProfile: JSON.stringify(req.oidc.user, null, 2),
    user: JSON.stringify(user, null, 2),
    title: 'Profile page'
  });
});

app.get('/settings', openid.requiresAuth(), async function (req, res, next) {
  const user = await getUser(req.oidc.user.sub)

  res.render('settings', {
    title: 'Settings page',
    attributes: user.attributes
  });
});

app.post('/add-attribute', openid.requiresAuth(), async function (req, res, next) {
  const { key, value } = req.body;
  const user = req.oidc.user

  const kcClient = await getKeycloakClient();
  const existingUser = await kcClient.get(`${config.keycloakUrl}/admin/realms/${config.realm}/users/${user.sub}`);

  const existingAttributes = existingUser.data.attributes || {};

  existingAttributes[key] = value;

  await kcClient.put(`${config.keycloakUrl}/admin/realms/${config.realm}/users/${user.sub}`, {
    ...existingUser.data,
    attributes: existingAttributes,
  });

  res.redirect('/settings');
});

app.post('/remove-attribute', openid.requiresAuth(), async function (req, res, next) {
  const { key } = req.body;
  const user = req.oidc.user

  const kcClient = await getKeycloakClient();
  const existingUser = await kcClient.get(`${config.keycloakUrl}/admin/realms/${config.realm}/users/${user.sub}`);

  const existingAttributes = existingUser.data.attributes || {};

  delete existingAttributes[key];

  await kcClient.put(`${config.keycloakUrl}/admin/realms/${config.realm}/users/${user.sub}`, {
    ...existingUser.data,
    attributes: existingAttributes,
  });

  res.redirect('/settings');
})

app.listen(9000, async () => {
  console.log('App listening on port 9000!')
  await setupKeycloak();
});

