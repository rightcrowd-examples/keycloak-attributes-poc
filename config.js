export const config = {
  keycloakUrl: 'https://iam.rightcrowd.dev',
  realm: 'ad-federation-test',
  apiUrl: `http://${process.env.HOST || 'localhost'}:9000`,
  apiClient: {
    clientId: 'keycloak-poc',
  },
  keycloakAdmin: {
    username: 'admin',
    password: process.env.KC_ADMIN_PASSWORD,
  }
}