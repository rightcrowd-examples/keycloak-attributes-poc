import { config } from './config.js'
import axios from 'axios';

export async function setupKeycloak() {
  console.log('Setting up Keycloak')
  await ensureClient();
}

export async function getKeycloakClient() {
  const tokenRes = await axios.post(`${config.keycloakUrl}/realms/master/protocol/openid-connect/token`, {
    grant_type: 'password',
    client_id: 'admin-cli',
    username: config.keycloakAdmin.username,
    password: config.keycloakAdmin.password,
  }, {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    }
  })

  return axios.create({
    headers: {
      Authorization: `Bearer ${tokenRes.data.access_token}`,
    }
  })
}

export async function getUser(id) {
  const client = await getKeycloakClient();
  const user = await client.get(`${config.keycloakUrl}/admin/realms/${config.realm}/users/${id}`);

  return user.data;
}

async function ensureClient() {
  const client = await getKeycloakClient();
  const currentClients = await client.get(`${config.keycloakUrl}/admin/realms/${config.realm}/clients`);

  const existingClient = currentClients.data.find(client => client.clientId === config.apiClient.clientId);

  if (existingClient) {
    return existingClient;
  }

  const newClient = await client.post(`${config.keycloakUrl}/admin/realms/${config.realm}/clients`, {
    clientId: config.apiClient.clientId,
    serviceAccountsEnabled: true,
    authorizationServicesEnabled: true,
    fullScopeAllowed: true,
    protocol: 'openid-connect',
    standardFlowEnabled: true,
    implicitFlowEnabled: true,
    directAccessGrantsEnabled: true,
    serviceAccountsEnabled: true,
    publicClient: false,
    redirectUris: [`${config.apiUrl}/callback`, `${config.apiUrl}`],
    webOrigins: [config.apiUrl],
  });

  return newClient.data;
}